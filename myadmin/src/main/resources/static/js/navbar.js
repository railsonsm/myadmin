var num = 50; // tanto de scroll que vai precisar para a barra ficar fixa.

$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num) {
        $('.fixed-top').css('top', 0);
    } else {
       //Quando o menu ficar fixo
        $('fixed-top').css('top', 130); 
    }
});

$(function() {
  $('#botton').click(function(){
    var href =$(this).attr("href");
    $('html').animate({
      scrollTop: $(href).offset().top
    }, 2500)
  })
});
