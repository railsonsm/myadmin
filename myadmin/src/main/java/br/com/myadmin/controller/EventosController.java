package br.com.myadmin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("eventos")
public class EventosController {
	
	@RequestMapping
	public String eventos() {
		return "/eventos/eventos"; 
	}
}
